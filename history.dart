class History {
  num first;
  num second;
  String operator;
  num result;
  History(num first, num second, String operator, num result) {
    this.first = first;
    this.second = second;
    this.operator = operator;
    this.result = result;
  }
  @override
  String toString() {
    return first.toString() +
        operator +
        second.toString() +
        "=" +
        result.toString();
  }
}
