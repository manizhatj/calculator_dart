import 'dart:io';
import 'calculator.dart';
import 'calculator_program.dart';

void main() {
//   // declare an instance of Calculator
//   var calculator = new Calculator();

// // test of +, *, -, / operators work
//   var addResult = calculator.add(2, 3);
//   var result = calculator.multiply(addResult, addResult);
//   print(result);
//   print(calculator.subtract(10, 10));
//   print(calculator.divide(10, 5));

//   // game of twenty four
//   var firstResult = calculator.subtract(3, 1);
//   var secondResult = calculator.subtract(8, firstResult);
//   var twentyFour = calculator.multiply(secondResult, 4);
//   print(twentyFour);

// // get input from user (always a String), parse it (to double), divide first number by second number, return the answer in double and in integer formats
//   stdout.writeln("First number");
//   var firstNumber = stdin.readLineSync();
//   stdout.writeln("Second number");
//   var secondNumber = stdin.readLineSync();

//   double firstNumberParsed = double.parse(firstNumber);
//   double secondNumberParsed = double.parse(secondNumber);

//   var answer = calculator.divide(firstNumberParsed, secondNumberParsed);
//   print(answer); // result in double
//   int endResult = answer.toInt();
//   print(endResult); // result in integer

// new version
  Calculator calculator = new Calculator();
  CalculatorProgram calculatorProgram = new CalculatorProgram(calculator);
  outer:
  while (true) {
    String choice = calculatorProgram.showChoices();
    Status status = calculatorProgram.acceptChoice(choice);
    switch (status) {
      case Status.history:
        calculatorProgram.printHistory();
        break;
      case Status.play:
        calculatorProgram.play();
        break;
      case Status.error:
        print("Invalid choice, please try again");
        break;
      case Status.quit:
        print("Goodbye");
        break outer;
    }
  }
}
