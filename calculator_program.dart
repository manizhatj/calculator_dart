import 'dart:io';
import 'calculator.dart';
import 'history.dart';

enum Status { history, play, quit, error }

class CalculatorProgram {
  final Calculator _calculator;

  CalculatorProgram(this._calculator);

  List<History> histories = [];
  String showChoices() {
    //show lines to add equation or print history
    //(if list is zero, don't show see history option)
    //return choice
    // if(history.length == 0){
    // ... }
    if (histories.length == 0) {
      stdout.writeln("Please type your choice below: Play | Quit");
    } else {
      stdout.writeln(
          "Please type your choice below: See History | Play Again | Quit");
    }
    var choice = stdin.readLineSync().toLowerCase();
    return choice;
  }

  Status acceptChoice(String choice) {
    //based on choice, start equation with asking two numbers and operation
    //or show the history of the calculator
    if (choice == 'see history') {
      return Status.history;
    } else if (choice == 'play' || choice == 'play again') {
      return Status.play;
    } else if (choice == 'quit') {
      return Status.quit;
    } else {
      return Status.error;
    }
  }

  void printHistory() {
    // for (int i = 0; i < histories.length; i++) {
    //   History history = histories.elementAt(i);
    //   print(history.first.toString() +
    //       history.operator +
    //       history.second.toString() +
    //       "=" +
    //       history.result.toString());
    // }

    print(histories);
  }

  void play() {
    stdout.writeln("Give me a number");
    String firstNumber = stdin.readLineSync();
    num first = num.parse(firstNumber);
    stdout.writeln("Give me another number");
    String secondNumber = stdin.readLineSync();
    num second = num.parse(secondNumber);
    String operator = askOperation();
    num result = calculate(first, second, operator);
    print(result);
    History history = new History(first, second, operator, result);
    histories.add(history);
  }

  num calculate(num first, num second, String operator) {
    switch (operator) {
      case "+":
        return _calculator.add(first, second);
        break;
      case "-":
        return _calculator.subtract(first, second);
        break;
      case "*":
        return _calculator.multiply(first, second);
        break;
      case "/":
        return _calculator.divide(first, second);
        break;
    }
  }

  String askOperation() {
    //ask the operation and execute the equation with the calculator
    stdout.writeln("Give me a an operator");
    var givenOperator = stdin.readLineSync();
    if (!(givenOperator == "+" ||
        givenOperator == "-" ||
        givenOperator == "*" ||
        givenOperator == "/")) {
      print("That is not a valid operator : " + givenOperator);
      return askOperation();
    }
    return givenOperator;
  }
}
